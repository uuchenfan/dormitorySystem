package test;

import cn.gok.entity.Student;
import cn.gok.service.impl.StudentServiceImpl;
import cn.gok.util.R;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

/**
 * @Author: chen fan
 * @Date: 2022/3/7 19:06
 * @Description: 学生个人信息模块相关的测试类
 */
public class StudentTest {

    StudentServiceImpl service;

    @Before
    public void init(){
        service = new StudentServiceImpl();
    }

    /**
     * 测试学生模块的登录操作
     */
    @Test
    public void test1(){
        R data = service.queryStudentByNameAndPassword("zhangsan", "123456");
        System.out.println(data);
    }

    /**
     * 测试修改密码模块
     */
    @Test
    public void test2() throws JsonProcessingException {

//        ObjectMapper mapper = new ObjectMapper();
//        String value = mapper.writeValueAsString(R.success("操作成功"));
//        System.out.println(value);

    }


    @Test
    public void test3() throws JsonProcessingException {


    }

}
