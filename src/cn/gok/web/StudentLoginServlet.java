package cn.gok.web;

import cn.gok.service.IStudentService;
import cn.gok.service.impl.StudentServiceImpl;
import cn.gok.util.Const;
import cn.gok.util.R;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.json.JSONUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @Author: chen fan
 * @Date: 2022/3/8 15:38
 * @Description:
 */
@WebServlet("/student")
public class StudentLoginServlet extends HttpServlet {

    private IStudentService studentService = new StudentServiceImpl();


    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action){
            case "login":login(req, resp);break;
            case "getVerifyCode": createVerifyCode(req, resp);break;
        }
    }


    public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //获取前端传入的验证码
        String verificationCode = req.getParameter("verificationCode");
        //获取系统中存储的验证码值
        String verifyCode = (String) req.getSession().getAttribute("verifyCode");

        if (!verifyCode.equals(verificationCode)){
            R r = R.fail("验证不正确，请重新输入",Const.VERIFY_COD_FAIL);
            resp.getWriter().println(JSONUtil.toJsonStr(r));
            return;
        }

        String studentName = req.getParameter("studentName");
        String password = req.getParameter("password");

        R data = studentService.queryStudentByNameAndPassword(studentName, password);


        if ((int)data.get(R.CODE) == Const.SUCCESS_CODE){
            req.getSession().setAttribute("student", data.get("student"));
        }
        String jsonStr = JSONUtil.toJsonStr(data);
        resp.getWriter().println(jsonStr);
    }

    /**
     * 本方法用于生产图形验证码
     */
    public void createVerifyCode(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(80, 40);

        //获取当前项目的根路径
        String contextPath = req.getSession().getServletContext().getRealPath("");

        //创建出验证码图片的存储路径,比如 /dormitorySystem/temp/verifyCodeImage
        File file = new File(contextPath + File.separator +"temp" + File.separator + "verifyCodeImage");

        //如果文件夹不存在，则创建
        if(!file.exists()){
            file.mkdirs();
        }

        //生成验证码图片的名字
        String verifyCodeName = UUID.randomUUID().toString()+".png";

        //生成验证码图片路径
        String verifyCodeImagePath = contextPath + File.separator +"temp"
                + File.separator + "verifyCodeImage" + File.separator + verifyCodeName;


        //图形验证码写出，可以写出到文件，也可以写出到流
        lineCaptcha.write(verifyCodeImagePath);

        //将验证码的值 存入session中
        req.getSession().setAttribute("verifyCode",lineCaptcha.getCode());


        //将验证码图片路径发送给前端以作显示
        R r = new R<>();
        r.put("verifyCodeName", verifyCodeName);

        resp.getWriter().println(JSONUtil.toJsonStr(r));
    }


}
