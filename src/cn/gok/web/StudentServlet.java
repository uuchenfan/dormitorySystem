package cn.gok.web;

import cn.gok.entity.Student;
import cn.gok.service.ISecurityQuestionService;
import cn.gok.service.IStudentService;
import cn.gok.service.impl.SecurityQuestionServiceImpl;
import cn.gok.service.impl.StudentServiceImpl;
import cn.gok.util.MapObjectMapper;
import cn.gok.util.R;
import cn.gok.util.TokenCacheUtil;
import cn.hutool.json.JSONUtil;
import lombok.Value;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @Author: chen fan
 * @Date: 2022/3/9 14:58
 * @Description:   学生个人信息处理模块
 */
@WebServlet("/studentInfo")
public class StudentServlet extends HttpServlet {


    private IStudentService studentService = new StudentServiceImpl();
    private ISecurityQuestionService securityQuestionService = new SecurityQuestionServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action){
            case "getStudentInfo":getStudentInfo(req, resp); break;
            case "updateStudentInfo": updateStudentInfo(req, resp); break;
            case "checkStudentName":checkStudentName(req, resp); break;
            case "checkPhoneNum":checkPhoneNum(req, resp);break;
            case "updatePassword":;
            case "getAllQuestion":showAllQuestion(req,resp);break;
            case "addSecurityQuestionAndAnswerInfo":addSecurityQuestionAndAnswerInfo(req, resp);break;
            case "setSecurityQuestion":;
        }
    }

    /**
      获取用户的基础信息
     */
    public void getStudentInfo(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Student student =  (Student) req.getSession().getAttribute("student");
        R data = R.createData(student);
        resp.getWriter().println(JSONUtil.toJsonStr(data));
    }

    /**
     * 检查用户名是否重复
     */
    public void checkStudentName(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String studentName = req.getParameter("studentName");
        int stuId = ((Student) req.getSession().getAttribute("student")).getStuId();
        R r = studentService.checkStudentNameRepeat(studentName, stuId);
        resp.getWriter().println(JSONUtil.toJsonStr(r));
    }

    /**
     * 检查手机号是否重复
     */
    public void checkPhoneNum(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String phoneNum = req.getParameter("phoneNum");
        int stuId = ((Student) req.getSession().getAttribute("student")).getStuId();
        R r = studentService.checkPhoneNumRepeat(phoneNum, stuId);
        resp.getWriter().println(JSONUtil.toJsonStr(r));
    }

    /**
     * 修改个人基本信息
     */
    public void updateStudentInfo(HttpServletRequest req, HttpServletResponse resp) throws IOException{

        Student student = MapObjectMapper.getObject(req, Student.class);
        Integer stuId = ((Student) req.getSession().getAttribute("student")).getStuId();
        student.setStuId(stuId);

        R r = studentService.updateStudentInfo(student);

        resp.getWriter().println(JSONUtil.toJsonStr(r));
    }


    /**
     * 展示所有的密保问题
     * @param req
     * @param resp
     */
    public void showAllQuestion(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        R allQuestion = securityQuestionService.getAllQuestion();
        resp.getWriter().println(JSONUtil.toJsonStr(allQuestion));
    }


    /**
     * 添加学生的密保问题及答案
     */
    public void addSecurityQuestionAndAnswerInfo(HttpServletRequest req, HttpServletResponse resp){


    }


    /**
     * 检查密保信息是否正在
     */
    public void checkSecurityQuestionAnswer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String studentName = req.getParameter("studentName");
        String question = req.getParameter("securityQuestion");
        String answer = req.getParameter("answer");

        R r = studentService.checkStudentSecurityQuestionAndAnswer(studentName, question, answer);

        resp.getWriter().println(JSONUtil.toJsonStr(r));
    }


    public void updatePassword_forgetPassword(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setHeader("Content-type", "application/json;charset=utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String token = req.getParameter("token");
        String studentName = req.getParameter("studentName");
        String newPassword = req.getParameter("newPassword");


        //1、如果未上传token，修改密码失败
        if (token == null || token == ""){
            resp.getWriter().println(JSONUtil.toJsonStr(R.fail("信息有误，修改密码失败")));
            return;
        }

        //2、验证用户上传的token是否正确

        String serverToken = TokenCacheUtil.getToken(TokenCacheUtil.TOKEN_ + studentName);

        //2.1、判断服务端的属于该用户的token是否存在
        if (serverToken == null){
            resp.getWriter().println(JSONUtil.toJsonStr(R.fail("信息有误，修改密码失败")));
            return;
        }

        //2.2、对比用户token是否正确
        if (!serverToken.equals(token)){
            resp.getWriter().println(JSONUtil.toJsonStr(R.fail("信息有误，修改密码失败")));
            return;
        }

        //3、token验证通过，则修改密码


    }


}
