package cn.gok.bo;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @Author: chen fan
 * @Date: 2022/3/15 11:20
 * @Description: 宿舍报修的BO对象，
 *  封装了 学生表、宿舍表、宿舍设备报修表的相关信息
 */
@Data
@ToString
public class MaintainDormitoryBO {

    /**
        报修信息的编号
     */
    private int id;

    /**
     * 报修学生的名字
     */
    private String realName;

    /**
     * 报修宿舍公寓名
     */
    private String apartmentName;

    /**
     * 公寓所在的位置信息
     */
    private String position;

    /**
     * 宿舍的门牌号
     */
    private int doorNum;


    /**
     * 报修的详细描述
     */
    private String desc;

    /**
     * 报修的状态
     */
    private int state;

    /**
     * 报修信息提交的时间
     */
    private Date createTime;

    /**
     * 报修状态修改的时间
     */
    private Date updateTime;



}
