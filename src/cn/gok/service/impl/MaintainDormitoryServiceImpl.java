package cn.gok.service.impl;

import cn.gok.bo.MaintainDormitoryBO;
import cn.gok.dao.MaintainDormitoryDAO;
import cn.gok.entity.MaintainDormitory;
import cn.gok.service.IMaintainDormitoryService;
import cn.gok.util.R;
import cn.gok.util.SqlSessionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.SqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: chen fan
 * @Date: 2022/3/11 11:05
 * @Description:
 */
public class MaintainDormitoryServiceImpl implements IMaintainDormitoryService {

    private SqlSession sqlSession;
    private MaintainDormitoryDAO maintainDormitoryDAO;

    public MaintainDormitoryServiceImpl(){
        sqlSession = SqlSessionUtil.getSqlSessionFactory().openSession();
        maintainDormitoryDAO = sqlSession.getMapper(MaintainDormitoryDAO.class);
    }


    @Override
    public R addMaintainDormitoryInfo(MaintainDormitory maintainDormitory) {
        int i = maintainDormitoryDAO.addMaintainDormitoryInfo(maintainDormitory);
        if (i > 0){
            return R.success();
        }
        return R.fail();
    }


    @Override
    public R queryMaintainDormitories(int stuId, int state, int currentPage, int pageSize) {

        //开启分页
        PageHelper.startPage(currentPage, pageSize);
        List<MaintainDormitoryBO> maintainDormitoryBOS = maintainDormitoryDAO.queryMaintainDormitories(stuId, state);

        PageInfo pageInfo = new PageInfo(maintainDormitoryBOS);
        
        /*
          需要向前端构造成对应的数据信息
            respData: {
                        result: ture, // 返回状态
                        msg: '',		// 返回信息
                        pagination: { // 翻页数据
                            currPage: 1,// 当前第几页
                            total: 100, //数据总数
                            pageSize: 10,  // 每页条数
                        },
                        datas: [    // 详细数据
                            {
                                id       //报修编号
                                realName //学生名字
                                apartmentName // 公寓名
                                        。。。
                            },
                            {
                                id       //报修编号
                                realName //学生名字
                                apartmentName // 公寓名
                                        。。。
                            }
                            ……
                        ],
                    }
         */

        R r = new R<>();
        r.put("result",true);
        r.put("msg","");

        Map<String, Object> pagination = new HashMap<>();
        pagination.put("currPage",currentPage);
        pagination.put("pageSize",pageSize);
        pagination.put("total",pageInfo.getTotal());

        r.put("pagination",pagination);
        r.put("datas",maintainDormitoryBOS);

        return r;
    }
}
