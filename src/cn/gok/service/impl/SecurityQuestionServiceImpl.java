package cn.gok.service.impl;

import cn.gok.dao.SecurityQuestionDAO;
import cn.gok.service.ISecurityQuestionService;
import cn.gok.util.R;
import cn.gok.util.SqlSessionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Author: chen fan
 * @Date: 2022/3/10 16:26
 * @Description:
 */
public class SecurityQuestionServiceImpl implements ISecurityQuestionService {
    private SqlSession sqlSession;
    private SecurityQuestionDAO securityQuestionDAO;

    public SecurityQuestionServiceImpl(){
        sqlSession = SqlSessionUtil.getSqlSessionFactory().openSession();
        securityQuestionDAO = sqlSession.getMapper(SecurityQuestionDAO.class);
    }


    @Override
    public R getAllQuestion() {
        List<String> allQuestion = securityQuestionDAO.getAllQuestion();
        return R.createData(allQuestion);
    }
}
