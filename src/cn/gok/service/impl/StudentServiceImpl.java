package cn.gok.service.impl;

import cn.gok.dao.StudentDAO;
import cn.gok.entity.Student;
import cn.gok.service.IStudentService;
import cn.gok.util.Const;
import cn.gok.util.R;
import cn.gok.util.SqlSessionUtil;
import cn.gok.util.TokenCacheUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.UUID;

/**
 * @Author: chen fan
 * @Date: 2022/3/7 19:25
 * @Description:
 */
public class StudentServiceImpl implements IStudentService {
    private SqlSession sqlSession;
    private StudentDAO studentDAO;

    public StudentServiceImpl(){
        sqlSession = SqlSessionUtil.getSqlSessionFactory().openSession();
        studentDAO = sqlSession.getMapper(StudentDAO.class);
    }


    @Override
    public R checkStudentSecurityQuestionAndAnswer(String studentName, String question, String answer) {
        int i = studentDAO.queryByStudentNameAndQuestionAndAnswer(studentName, question, answer);

        if (i > 0){
           //如果密保验证成功，生成一份对应的token，分别缓存在服务中和发送给前端

            //生成token
            String tokenValue = UUID.randomUUID().toString();

            //将 token在本地服务器缓存一份
            TokenCacheUtil.putToken(TokenCacheUtil.TOKEN_ + studentName, tokenValue);

            //将token存入响应结果集，发送给前端页面
            R r = R.success("密保验证成功");
            r.put("token", tokenValue);
            return r;
        }
        return R.fail("密保验证失败");
    }

    @Override
    public R updateStudentInfo(Student student) {
        int i = studentDAO.updateStudentInfo(student);
        if ( i > 0 ){
            return  R.success("修改成功");
        }
        return R.fail("修改失败");
    }

    /**
     *  根据用户名和密码查询用户
     * @param studentName
     * @param password
     * @return
     */
    @Override
    public R queryStudentByNameAndPassword(String studentName, String password) {

        Student student = studentDAO.queryStudentByNameAndPassword(studentName, password);
        if (student != null){
            R data = R.success(Const.LOGIN_SUCCESS_MSG);
            data.put("student",student);
            return data;
        }
        return R.fail(Const.LOGIN_FAIL_MSG);
    }

    /**
     * 根据id查询个人用户信息
     * @param id
     * @return
     */
    @Override
    public Student queryStudentById(int id) {
        return studentDAO.queryStudentById(id);
    }

    /**
     * 检查用户名是否存在
     * @param studentName
     * @param stuId
     * @return
     */
    @Override
    public R checkStudentNameRepeat(String studentName, int stuId) {
        return checkStudentNameOrPhoneNumRepeat(studentName,null,stuId);
    }

    /**
     * 检查手机号是否存在
     * @param phoneNum
     * @param stuId
     * @return
     */
    @Override
    public R checkPhoneNumRepeat(String phoneNum, int stuId) {
        return checkStudentNameOrPhoneNumRepeat(null,phoneNum,stuId);
    }

    /**
     *  检查用户名 或手机号是否重写
     * @param studentName
     * @param phoneNum
     * @param stuId
     * @return
     */
    private R checkStudentNameOrPhoneNumRepeat(String studentName, String phoneNum, int stuId) {
        int i = studentDAO.checkStudentNameOrPhoneNumRepeat(studentName, phoneNum, stuId);
        if (i > 0){
            return R.fail();
        }
        return R.success();
    }


}
