package cn.gok.service;

import cn.gok.entity.MaintainDormitory;
import cn.gok.util.R;

/**
 * @Author: chenfan
 * @Description:
 */
public interface IMaintainDormitoryService {


    R addMaintainDormitoryInfo(MaintainDormitory maintainDormitory);

    R queryMaintainDormitories(int stuId,int state,int currentPage,int pageSize);
}
