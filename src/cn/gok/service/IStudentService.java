package cn.gok.service;

import cn.gok.entity.Student;
import cn.gok.util.R;

/**
 * @Author: chenfan
 * @Description:
 */
public interface IStudentService {

    R queryStudentByNameAndPassword(String studentName, String password);

    Student queryStudentById(int id);


    R checkStudentNameRepeat(String studentName,int stuId);

    R checkPhoneNumRepeat(String phoneNum,int stuId);


    R updateStudentInfo(Student student);

    R checkStudentSecurityQuestionAndAnswer(String studentName,String question,String answer);
}
