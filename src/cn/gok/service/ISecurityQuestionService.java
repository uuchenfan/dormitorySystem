package cn.gok.service;

import cn.gok.util.R;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:
 */
public interface ISecurityQuestionService {

    /**
     * 获取所有的密保问题
     * @return
     */
    R getAllQuestion();

}
