package cn.gok.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Dormitory)宿舍信息实体类
 *
 * @author makejava
 * @since 2022-03-11 10:57:19
 */
public class Dormitory implements Serializable {
    private static final long serialVersionUID = -75676948283760286L;
    /**
     * 主键
     */
    private Integer dorId;
    /**
     * 宿舍位置关联id
     */
    private Integer dorPosId;
    /**
     * 楼层
     */
    private Integer floor;
    /**
     * 门牌号
     */
    private Integer doorNum;
    /**
     * 最大床位数
     */
    private Integer maxNum;
    /**
     * 剩余床位数
     */
    private Integer surplusNum;
    /**
     * 宿舍类型： 2： 两人间  4： 4人间   6：六人间
     */
    private Integer type;
    /**
     * 0： 空寝室   1：已入住（男寝）  2：已入住（女寝）
     */
    private Integer state;
    /**
     * 添加此宿舍的管理员id
     */
    private Integer managerId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;


    public Integer getDorId() {
        return dorId;
    }

    public void setDorId(Integer dorId) {
        this.dorId = dorId;
    }

    public Integer getDorPosId() {
        return dorPosId;
    }

    public void setDorPosId(Integer dorPosId) {
        this.dorPosId = dorPosId;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getDoorNum() {
        return doorNum;
    }

    public void setDoorNum(Integer doorNum) {
        this.doorNum = doorNum;
    }

    public Integer getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Integer maxNum) {
        this.maxNum = maxNum;
    }

    public Integer getSurplusNum() {
        return surplusNum;
    }

    public void setSurplusNum(Integer surplusNum) {
        this.surplusNum = surplusNum;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
