package cn.gok.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (MaintainDormitory)宿舍设备报修实体类
 *
 * @author makejava
 * @since 2022-03-11 10:57:19
 */
public class MaintainDormitory implements Serializable {
    private static final long serialVersionUID = -81412774826103546L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 上报此宿舍报修信息的学生id
     */
    private Integer stuId;
    /**
     * 报修的宿舍id
     */
    private Integer dorId;

    private String desc;

    private String imageUrl;
    /**
     * 0: 未处理  1：处理中 2：已处理
     */
    private Integer state;

    private Date createTime;

    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    public Integer getDorId() {
        return dorId;
    }

    public void setDorId(Integer dorId) {
        this.dorId = dorId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
