package cn.gok.entity;

import java.io.Serializable;

/**
 * (ManagerInfo)实体类
 *
 * @author makejava
 * @since 2022-03-07 19:04:03
 */
public class ManagerInfo implements Serializable {
    private static final long serialVersionUID = -38690359133017403L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 电话
     */
    private String phoneNum;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 性别
     */
    private String gender;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
