package cn.gok.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.io.Serializable;

/**
 * (Student)实体类
 *
 * @author makejava
 * @since 2022-03-07 19:04:04
 */
@Data
@ToString
public class Student implements Serializable {
    private static final long serialVersionUID = -78948787806849387L;
    /**
     * 主键
     */
    private Integer stuId;
    /**
     * 学生的用户名
     */
    private String stuName;
    /**
     * 密码
     */
    private String password;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 手机号
     */
    private String phoneNum;
    /**
     * 学校
     */
    private String schoolName;
    /**
     * 锁定状态：
     *   0正常     1锁定
     */
    private Integer lockState;
    /**
     * 锁定时间
     */
    private Date lockTime;
    /**
     * 登录时间
     */
    private Date loginTime;
    /**
     * 失败计数，默认初值为5
        在登录/密保验证时，如果操作失败5次，则冻结账号24小时；
        非锁定账号，每天6点刷新失败次数。
     */
    private Integer failCount;


}
