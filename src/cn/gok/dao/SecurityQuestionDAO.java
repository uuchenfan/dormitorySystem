package cn.gok.dao;

import java.util.List;

/**
 * @Author: chen fan
 * @Date: 2022/3/10 16:19
 * @Description:  操作安全问题的dao
 */
public interface SecurityQuestionDAO {


    /**
     * 获取所有的密保问题
     * @return
     */
    List<String> getAllQuestion();


    /**
     *  添加密保问题
     */
    int addQuestion(String question);
}
