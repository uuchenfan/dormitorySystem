package cn.gok.dao;

import cn.gok.bo.MaintainDormitoryBO;
import cn.gok.entity.MaintainDormitory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:  宿舍设备报修持久层接口
 */
public interface MaintainDormitoryDAO {

    int addMaintainDormitoryInfo(MaintainDormitory maintainDormitory);

    /**
     * 根据学生的id 和 报修的状态获取 报修列表信息
     *   个人编号、姓名、 宿舍位置、宿舍楼层、宿舍编号、 报修信息、报修状态、提报时间
     */

    List<MaintainDormitoryBO> queryMaintainDormitories(@Param("stuId") int stuId,
                                                       @Param("state") int state);
}
