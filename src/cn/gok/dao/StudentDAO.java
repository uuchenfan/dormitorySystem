package cn.gok.dao;

import cn.gok.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:   学生持久层接口
 */
public interface StudentDAO {

    /**
     *  根据用户名和密码获取用户信息
     * @param stuName
     * @param password
     * @return
     */
    Student queryStudentByNameAndPassword(@Param("stuName") String stuName, @Param("password") String password);

    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
    Student queryStudentById(int id);

    /**
     *  检查学生的用户名和手机号是否重复
     * @param studentName
     * @param phoneNum
     * @return
     */
    int checkStudentNameOrPhoneNumRepeat(@Param("studentName") String studentName,
                                         @Param("phoneNum") String phoneNum,
                                         @Param("stuId") int stuId);

    /**
     * 修改学生基础信息
     * @param student
     * @return
     */
    int updateStudentInfo(Student student);


    /**
     * 根据用户名 、密保问题、密保答案 查询信息
     * @param studentName
     * @param question
     * @param answer
     * @return
     */
    int queryByStudentNameAndQuestionAndAnswer(@Param("studentName") String studentName,
                                               @Param("question") String question,
                                               @Param("answer") String answer);

}
