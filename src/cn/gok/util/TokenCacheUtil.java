package cn.gok.util;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: chen fan
 * @Date: 2022/3/10 20:34
 * @Description: token缓存的工具类
 */
public class TokenCacheUtil {
    public static final String TOKEN_ = "TOKEN_";

    /**
     * 创建一个TimedCache缓存对象，并设置时间为5分钟过期
     */
    public static TimedCache<String,String> tokenCache = CacheUtil.newTimedCache(1000*60*5);


    /**
     * 向缓存中添加数据
     * @param tokenName
     * @param tokenValue
     */
    public static void putToken(String tokenName,String tokenValue){
        tokenCache.put(tokenName, tokenValue);
    }

    /**
     * 根据tokenName获取token
     * @param tokenName
     * @return
     */
    public static String getToken(String tokenName){
        return tokenCache.get(tokenName);
    }
    /**
     * 删除缓存中的数据
     * @param tokenName
     */
    public static void removeToken(String tokenName){
        tokenCache.remove(tokenName);
    }

}
