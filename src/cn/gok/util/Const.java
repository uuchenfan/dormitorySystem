package cn.gok.util;

/**
 * @Author: chen fan
 * @Date: 2022/3/8 17:06
 * @Description:
 */
public final class Const {
    public static final int SUCCESS_CODE = 200;
    public static final int FAIl_CODE = 0;
    public static final int VERIFY_COD_FAIL = 10000;


    public static final String SUCCESS_OPERATION_MSG = "操作成功";
    public static final String FAIL_OPERATION_MSG = "操作成功";
    public static final String LOGIN_SUCCESS_MSG = "登录成功";
    public static final String LOGIN_FAIL_MSG = "用户名或密码错误，登录失败";
}
