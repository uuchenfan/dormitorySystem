package cn.gok.util;

import java.lang.instrument.ClassDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: chen fan
 * @Date: 2022/3/8 16:45
 * @Description:  自定义的一个相应结果集，用来封装响应给前端的数据
 */
public class R<T> extends HashMap {

    /**
     * 给前端的提示信息
     */
    private String msg;
    /**
     * 给前端的响应状态码
     */
    private int code;

    /**
     * 给前端的响应数据
     */
    private T data;

    /**
     * 设置msg的键名
     */
    public static final String CODE = "code";
    /**
     * 设置MSG的键名
     */
    public static final String MSG = "data";
    /**
     * 设置DATA的键名
     */
    public static final String DATA = "data";


    public R() {

    }

    public R(String msg, int code, T data) {
        super.put("msg", msg);
        super.put("code", code);
        super.put("data", data);
    }


    /**
     * 当操作成功，直接调用此方法获取响应成功信息
     * @return
     */
    public static R success(){
        return new R(Const.SUCCESS_OPERATION_MSG, Const.SUCCESS_CODE, null);
    }

    /**
     * 当操作成功，响应消息，并且不需要响应数据时，可以直接调用此方法
     * @param msg  成功信息
     * @return
     */
    public static R success(String msg){
        return new R(msg, Const.SUCCESS_CODE, null);
    }

    /**
     * 操作成功，并且返回数据时调用此方法
     * @param data 响应给前端的数据
     * @return
     */
    public static R createData(Object data){
        return new R(null, Const.SUCCESS_CODE, data);
    }

    /**
     * 当操作失败，直接调用本方法创建失败信息
     * @return
     */
    public static R fail(){
        return new R(Const.FAIL_OPERATION_MSG, Const.FAIl_CODE, null);
    }

    /**
     * 当操作失败，并提示信息时，可以直接调用此方法
     * @param msg  失败信息
     * @return
     */
    public static R fail(String msg){
        return new R(msg, Const.FAIl_CODE, null);
    }

    public static R fail(String msg,int code){
        return new R(msg, code, null);
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
