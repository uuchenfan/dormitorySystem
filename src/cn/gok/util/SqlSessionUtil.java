package cn.gok.util;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * @Author: chen fan
 * @Date: 2022/3/7 19:45
 * @Description:
 */
public class SqlSessionUtil {

    private SqlSessionUtil(){};

    private volatile static SqlSessionFactory sessionFactory;

    /**
     * 双检锁/双重校验锁（DCL，即 double-checked locking）线程安全，懒加载
     * @return
     */
    public static SqlSessionFactory getSqlSessionFactory(){
        if(sessionFactory == null){
            synchronized (SqlSessionUtil.class){
                if(sessionFactory == null){
                    InputStream is = SqlSessionUtil.class.getClassLoader().getResourceAsStream("mybatis-config.xml");
                    sessionFactory = new SqlSessionFactoryBuilder().build(is);
                }
            }
        }
        return sessionFactory;
    }
}



